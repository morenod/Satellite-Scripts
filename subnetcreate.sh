#!/bin/bash
username=
password=

ayuda ()
{
        echo "Usage: subnetcreate [OPTIONS]"
        echo "This script uses hammer to create a subnet in Satellite"
        echo " "
        echo "  --name NAME             	Name of the subnet"
        echo "  --domain DOMAIN         	Satellite Domain where subnet has to be included"
        echo "  --network NETWORK              	Network to add, for example 192.168.0.0/25"
	echo "	--dns (Optional)		IP Addresses of DNS for the Network, separated with comma"
	echo "	--range	(Optional)		IP Range for DHCP, separated with comma"
	echo "	--vlanid (Optional)		VLAN ID of the Network"
	echo "	--pxe (Optional)		Configure this network for PXE deployments"
        echo " "
        echo "  --help                  	Display this help and exit"
        echo "  --version               	Print version information and exit"
        echo " "
        echo "Report bugs to dasanz@ext.produban.com"
}

error ()
{
        echo "Usage: subnetcreate [OPTIONS]"
        echo "Try \`subnetcreate --help' for more information."
}

version ()
{
        echo "subnetcreate (IaaS Produban Utils) 1.0"
        echo " "
        echo "Copyright (C) 2015 Free Software Foundation, Inc."
        echo "This is free software.  You may redistribute copies of it under the terms of"
        echo "the GNU General Public License <http://www.gnu.org/licenses/gpl.html>."
        echo "There is NO WARRANTY, to the extent permitted by law."
        echo " "
        echo "Written by David Sanz"
}

#Busca si hay una capsula asociada al dominio cuando se configura la red para PXE
capsule_search()
{
	capsule_id=$(hammer -u $username -p $password --output csv capsule list | grep $1 | awk -F, '{print $1}')
	if [ "${capsule_id:-"null"}" == null ] ; then
		echo "No capsule found for domain $domain"
		exit 1
	fi
	capsule=$(echo $capsule_id | awk '{if ($1) print "--dhcp-id "$1" --tftp-id "$1}')
}

#Partiendo de una red tipo 192.168.0.0/25, calcula la direccion de red, la mascara y el gateway
#Se toma como premisa que el gateway es siempre la primera IP de la red
net_creation()
{
	network=$(ipcalc $1 -n | awk -F= '{print $2}')
	netmask=$(ipcalc $1 -m | awk -F= '{print $2}')
	gateway=$(expr $(echo $network | awk -F. '{print $4}') + 1)
	gateway=$(echo $network | awk -F. -v x=$gateway '{print $1"."$2"."$3"."x}')
}

#Busca si el dominio indicado existe
search_domain_id()
{
	domain_id=$(hammer -u $username -p $password --output csv domain list | grep $1 | awk -F, '{print $1}')
	if [ "${domain_id:-"null"}" == null ] ; then
                echo "Domain $domain not found"
                exit 1
        fi
}

#Recibe un rango y lo desgrana
calcular_rango()
{
	from=$(echo $1 | awk -F, '{print "--from "$1}')
	to=$(echo $1 | awk -F, '{if ($2) print "--to "$2}')

}

#En funcion del numero de IPs, configura un dns o dos
calcular_dns()
{
        dns1=$(echo $1 | awk -F, '{print "--dns-primary "$1}')
        dns2=$(echo $1 | awk -F, '{if ($2) print "--dns-secondary "$2}')

}

###################### PARAMETERS CONTROL #######################

if [ $# -eq 0 ] ; then
        error ; exit 1
fi

while [ $# -gt 0 ]; do
        case $1 in
                --help) ayuda ; exit 0;;
                --version) version ; exit 0;;
                --name)      shift ; name=$1 ;;
                --domain)       shift ; domain=$1 ; search_domain_id $1 ;;
		--network) shift ; net_creation $1 ;;
                --vlanid) shift ; vlanid=$(echo $1 | awk -F, '{if ($1) print "--vlanid "$1}') ;;
		--dns) shift ; calcular_dns $1 ;;
		--range) shift ; calcular_rango $1 ;;
		--pxe) capsule_search $domain ;;
                *)  error ; exit 1 ;;

        esac
        shift
done

location_id=$(hammer -u $username -p $password --output csv location list | grep `echo ${name:0:4}` | awk -F, '{print $1}')

#Control de que existe los parametros obligatorios para que el script funcione
if [ "${name:-"null"}" == null -o "${domain_id:-"null"}" == null -o "${network:-"null"}" == null -o "${gateway:-"null"}" == null -o "${netmask:-"null"}" == null ] ; then error ; exit 1 ; fi

hammer  -u $username -p $password subnet create --name $name --network $network --mask $netmask --gateway $gateway $dns1 $dns2 $from $to $vlanid --domain-ids $domain_id $capsule --location-ids $location_id --organizations IaaS
