#!/bin/bash
username=
password=
################## FUNCTIONS ##################

ayuda ()
{
        echo "Usage: hostcreate [OPTIONS]"
        echo "This script uses hammer to create a host in Satellite"
        echo " "
        echo "  --name NAME             	Hostname of the host (without domain)"
        echo "  --domain DOMAIN         	Satellite Domain where server has to be included"
	echo "  --location LOCATION		IaaS Region (Default: None)"
	echo "  --organization ORGANIZATION	Satellite Organization (Default: None)"
        echo "  --ip IP                 	Address used for PXE deployment and primary interface of the server"
        echo "  --mac                   	HW address of the primary interface"
        echo "  --hostgroup HOSTGROUPID		Hostgroup used to configure the server"
        echo "  --interface INTERFACE   	Any other interface of the server"
        echo "        INTERFACE FORMAT: \"<<IP Address>>|<<MAC Address>>\""
        echo " "
        echo "  --help                  	Display this help and exit"
        echo "  --version               	Print version information and exit"
        echo " "
        echo "Report bugs to dasanz@ext.produban.com"
}

error ()
{
        echo "Usage: hostcreate [OPTIONS]"
        echo "Try \`hostcreate --help' for more information."
}

version ()
{
        echo "hostcreate (IaaS Produban Utils) 1.0"
        echo " "
        echo "Copyright (C) 2015 Free Software Foundation, Inc."
        echo "This is free software.  You may redistribute copies of it under the terms of"
        echo "the GNU General Public License <http://www.gnu.org/licenses/gpl.html>."
        echo "There is NO WARRANTY, to the extent permitted by law."
        echo " "
        echo "Written by David Sanz"
}

search_subnet_id()
{
        #Recogemos toda la lista de redes dadas de alta en formato <<network>>/<<mask>>
        for i in `hammer -u ${username} -p ${password} --csv subnet list | awk -F, '{print $3"/"$4}' | tail -n +2` ; do
                network=$(echo $i | awk -F/ '{print $1}')
                netmask=$(echo $i | awk -F/ '{print $2}')
                ip_network=$(ipcalc $1 $netmask -n | awk -F= '{print $2}')
                if [ $network == $ip_network ] ; then
                        echo $(hammer -u ${username} -p ${password} --csv subnet list | grep $network | grep $netmask | awk -F, '{print $1}')
                        return 0
                fi
        done
        return 1
}

search_hostgroup_id()
{
	int_name=$3
	if [[ $int_name == SRV* ]] ; then
		region=${int_name:3:4}
		az=${int_name:7:2}
                serverNumber=$(( 10#${name: -2} ))
                rack=$(( $serverNumber / 24 + $([[ $(( $serverNumber % 24 )) -eq 0 ]] && echo 0 || echo 1) ))
	else
		region=${int_name:2:4}
                az=${int_name:6:2}
                serverNumber=$(( 10#${name: -2} ))
                rack=$(( $serverNumber / 24 + $([[ $(( $serverNumber % 24 )) -eq 0 ]] && echo 0 || echo 1) ))
	fi
	if [ $az == "00" ] ; then
		hostgroup_id=$(hammer -u ${username} -p ${password} --csv hostgroup list | grep $1 | grep $region | grep INFRA | awk -F, '{print $1}')
	else
		hostgroup_id=$(hammer -u ${username} -p ${password} --csv hostgroup list | grep $1 | grep $region | grep AZ$az | grep RACK$rack | awk -F, '{print $1}')
	fi
	echo $hostgroup_id
	return 0
}
	

###################### PARAMETERS CONTROL #######################

if [ $# -eq 0 ] ; then
        error ; exit 1
fi

while [ $# -gt 0 ]; do
        case $1 in
                --help) ayuda ; exit 0;;
                --version) version ; exit 0;;
                --name)      shift ; name=$1 ;;
                --ip)        shift ; 
                        main_ip=$1 ;
                        main_subnet_id=$(search_subnet_id $main_ip)
                        if [ $? -ne 0 ] ; then
                                echo "Subnet not found for $main_ip address"
                                exit 1
                        fi
                        ;;
                --mac)       shift ; mac=$1 ;;
                --hostgroup) shift ;
                        hostgroup_name=$1 ;;
                --domain)    shift ;
			domain_name=$1
			domain=$(hammer -u ${username} -p ${password} --csv domain list | grep "${1}" | awk -F, '{print $1}')
			if [ "${domain:-"null"}" == null ] ; then
				echo "Domain $1 not found"
				exit 1
			fi ;;
                --interface) shift ; interfaces+=($1) ;;
		--location) shift ;
			hammer -u ${username} -p ${password} location info --name $1 >/dev/null 2>&1
			if [ $? -ne 0 ] ; then
				echo "Location $1 not found"
				exit 1
			else
				location_id=$(hammer -u ${username} -p ${password} --csv location list | grep ",${1}$"| awk -F, '{print $1}')
			fi
			;;
                --organization) shift ;
                        hammer -u ${username} -p ${password} organization info --name $1 >/dev/null 2>&1
                        if [ $? -ne 0 ] ; then
                                echo "Organization $1 not found"
                                exit 1
                        else
                                organization_id=$(hammer -u ${username} -p ${password} --csv organization list | grep ",${1},"| awk -F, '{print $1}')
                        fi
                        ;;

                *)  error ; exit 1 ;;

        esac
        shift
done

hostgroup=$(search_hostgroup_id $hostgroup_name $domain_name $name)
domain=$(hammer -u ${username} -p ${password} --csv domain list | grep "$domain_name" | awk -F, '{print $1}')

if [ "${name:-"null"}" == null -o "${main_ip:-"null"}" == null -o "${mac:-"null"}" == null -o "${hostgroup:-"null"}" == null -o "${domain:-"null"}" == null -o "${location_id:-"null"}" == null -o "${organization_id:-"null"}" == null ] ; then error ; exit 1 ; fi

#################################################################

for i in "${interfaces[@]}"; do
        nip=`echo $i | awk -F\| '{print $1}'`
        nmac=`echo $i | awk -F\| '{print $2}'`
        subnet_id=$(search_subnet_id "$nip")
        if [ $? -ne 0 ] ; then
                echo "Subnet not found for $nip address"
                exit 1
        fi
        options+="--interface=type=Nic::Managed,mac=$nmac,domain_id=$domain,subnet_id=$subnet_id,ip=$nip "
done

echo hammer -u ${username} -p ${password} host create --name $name --organization-id $organization_id --location-id $location_id --hostgroup-id $hostgroup --mac $mac --ip $main_ip --subnet-id $main_subnet_id --domain-id $domain $options
